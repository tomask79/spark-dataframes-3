# Apache Spark and DataFrames 3 #

DataFrames in general are great for manipulating the Big Data, a lot of times you want to work with large XML files
stored at the Hadoop HDFS for example. Transforming XML into dataframe by your own Spark program is possible, but using 
a third party tools like [Spark-XML](https://github.com/databricks/spark-xml) library from databricks is far more 
effecient.

## Using of Databricks library to analyze Big Data XML files ##

Read carefully link provided in the previous article. I will mention the most important things:

### Schema of XML ###

There are two options how you can define a schema for Spark-XML:

* By defining a rowTag option (schema will be figured out according the subelements or attributes)
* By defining the schema manually like:

```
val sqlContext = new SQLContext(sc)
val customSchema = StructType(Array(
    StructField("_id", StringType, nullable = true),
    StructField("author", StringType, nullable = true),
    StructField("description", StringType, nullable = true),
    StructField("genre", StringType ,nullable = true),
    StructField("price", DoubleType, nullable = true),
    StructField("publish_date", StringType, nullable = true),
    StructField("title", StringType, nullable = true)))


val df = sqlContext.read
    .format("com.databricks.spark.xml")
    .option("rowTag", "book")
    .schema(customSchema)
    .load("books.xml")
```
In real production applications I'd definitelly go with second option. In the demo let's
test the first option:

### Analysing the Big Data XML with DataFrame ###

Let's have following example file *persons.xml*

```
<persons>
  <person>
    <name>John</name>
    <department>consultant</department>
  </person>
  <person>
    <name>Marc</name>
    <department>consultant</department>
  </person>
  <person>
    <name>Brock</name>
    <department>IT</department>
  </person>
</persons>
```

Task: Get all persons from department = 'consultant'.
Solution via databricks library (scala):

```
import org.apache.spark.sql.SQLContext;

val df = sqlContext.read.format("com.databricks.spark.xml").option("rowTag", "person").load("/spark/persons.xml");

df.printSchema();

df.registerTempTable("persons");

var consDF = sqlContext.sql("select * from persons where department = 'consultant'");

consDF.show();
```

Now run the following from hortonworks sandbox:

```
[root@sandbox spark-dataframes-3]# spark-shell --packages com.databricks:spark-xml_2.10:0.4.1 -i XMLDataFrame.scala 
```

Output should be:

```
df: org.apache.spark.sql.DataFrame = [department: string, name: string]
root
 |-- department: string (nullable = true)
 |-- name: string (nullable = true)
 .
 .
17/07/19 00:44:04 INFO DAGScheduler: Job 1 finished: show at <console>:29, took 0,338165 s
+----------+----+
|department|name|
+----------+----+
|consultant|John|
|consultant|Marc|
+----------+----+
 
```

Schema will be read from the structure of the first appearence of the rowTag person.
Now what if some of person tags will be broken? Well databricks spark-xml library works in
three modes:

* PERMISSIVE : When there will be broken record, library puts null values in that place.
* DROPMALFORMED : Whole rowTag is ignored.
* FAILFAST : Library stops the processing if there is broken record.

I pretty much like spark-xml library from databricks. Definitelly use it if you're having
large Big Data XML files at Hadoop storage and you need to analyze it.

regards

Tomas

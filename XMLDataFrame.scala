import org.apache.spark.sql.SQLContext;

val df = sqlContext.read.format("com.databricks.spark.xml").option("rowTag", "person").load("/spark/persons.xml");

df.printSchema();

df.registerTempTable("persons");

var consDF = sqlContext.sql("select * from persons where department = 'consultant'");

consDF.show();
